﻿using UnityEngine;

namespace Itorum.UI
{
	public class CursorController : MonoBehaviour
	{
		private Managers.ActionsManager actionsManager = null;

		private void Start()
		{
			UnityEngine.Cursor.visible = false;
			UnityEngine.Cursor.lockState = CursorLockMode.Locked;

			actionsManager = Managers.ActionsManager.Instance;

			if (actionsManager)
			{ actionsManager.Actions.Keyboard.UnlockCursor.performed += context => UpdateCursorState(); }
			else
			{ Debug.LogError("ActionsManager is null"); }
		}

		private void UpdateCursorState()
		{
			UnityEngine.Cursor.visible = !UnityEngine.Cursor.visible;
			UnityEngine.Cursor.lockState =
				UnityEngine.Cursor.lockState == CursorLockMode.Locked ?
				CursorLockMode.None :
				CursorLockMode.Locked;
		}
	}
}