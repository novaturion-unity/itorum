﻿using System.Collections;
using UnityEngine;

namespace Itorum.Managers
{
	public class UIManager : MonoBehaviour
	{
		public static UIManager Instance { get; private set; }

		public int missileCameraIndex = 0;
		public int airplaneCameraIndex = 0;

		public Cameras.CameraSwitcher cameraSwitcher = null;

		public UI.LaunchPanelController launchPanelController = null;
		public UI.CamerasPanelController camerasPanelController = null;

		public GameObject messagePanel = null;

		public TMPro.TextMeshProUGUI errorText = null;
		public TMPro.TextMeshProUGUI hittedText = null;

		private ActorsManager actorsManager = null;
		private Managers.ActionsManager actionsManager = null;
		private void Awake()
		{
			if (Instance == null)
			{ Instance = this; }
			else if (Instance == this)
			{ Destroy(this.gameObject); }
		}

		private void Start()
		{
			actorsManager = Managers.ActorsManager.Instance;
			actionsManager = Managers.ActionsManager.Instance;

			actionsManager.Actions.Keyboard.LaunchMissile.performed += context => OnLaunchButtonClick();
			actionsManager.Actions.Keyboard.SelectFirstCamera.performed += context => OnCameraButtonClick(0);
			actionsManager.Actions.Keyboard.SelectSecondCamera.performed += context => OnCameraButtonClick(1);
			actionsManager.Actions.Keyboard.SelectThirdCamera.performed += context => OnCameraButtonClick(2);
		}

		public void OnLaunchButtonClick()
		{
			if (!launchPanelController.gameObject.activeSelf)
			{ return; }

			if (launchPanelController.IsValid && !launchPanelController.IsPrepared)
			{ SetPrepared(true); }
			else if (launchPanelController.IsValid && launchPanelController.IsPrepared)
			{
				actorsManager.LaunchMessile();
				SetPrepared(false);
				SetLaunchControlsVisibility(false);
				SetCameraControlsVisibility(true);
			}
			else
			{ ShowPreparingError(); }
		}

		public void OnCameraButtonClick(int index)
		{
			if (!camerasPanelController.gameObject.activeSelf)
			{ return; }

			cameraSwitcher.Switch(index);
			camerasPanelController.SwitchCameraButton(index);
		}

		public void SetValidity(bool state)
		{
			if (state && !launchPanelController.IsValid)
			{
				launchPanelController.IsValid = true;
				launchPanelController.SetValidText();
			}
			else if (!state && launchPanelController.IsValid)
			{
				launchPanelController.IsValid = false;
				launchPanelController.SetInvalidText();
			}
		}

		public void SetPrepared(bool state)
		{
			if (state && !launchPanelController.IsPrepared)
			{
				launchPanelController.IsPrepared = true;
				launchPanelController.SetLaunchText();
				actorsManager.SpawnMissile();
			}
			else if (!state && launchPanelController.IsPrepared)
			{
				launchPanelController.IsPrepared = false;
				launchPanelController.SetPrepareText();
			}
		}

		public void SetMissileCamera(Camera camera)
		{ cameraSwitcher.SetCamera(missileCameraIndex, camera); }

		public void SetAirplaneCamera(Camera camera)
		{ cameraSwitcher.SetCamera(airplaneCameraIndex, camera); }

		public void ShowPreparingError()
		{ StartCoroutine(ShowMessageCoroutine(messagePanel, errorText)); }

		public void ShowHittedMessage()
		{ StartCoroutine(ShowMessageCoroutine(messagePanel, hittedText)); }

		public void SetLaunchControlsVisibility(bool state)
		{ launchPanelController.gameObject.SetActive(state); }

		public void SetCameraControlsVisibility(bool state)
		{ camerasPanelController.gameObject.SetActive(state); }

		public void ActivateAirplaneCamera()
		{ cameraSwitcher.Switch(airplaneCameraIndex); }

		public void ActivateDefaultCamera()
		{ cameraSwitcher.Switch(0); }

		private IEnumerator ShowMessageCoroutine(GameObject messagePanel, TMPro.TextMeshProUGUI panelText)
		{
			messagePanel.SetActive(true);
			panelText.gameObject.SetActive(true);

			yield return new WaitForSeconds(5f);

			messagePanel.SetActive(false);
			panelText.gameObject.SetActive(false);

			yield break;
		}
	}
}